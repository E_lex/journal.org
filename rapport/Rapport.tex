%!TEX encoding = UTF-8 Unicode

% rubber: shell_escape
% rubber: path ./imgs//
% rubber: path ./sty//


% =============================================================================

\documentclass[%
11pt,
final,
english,french,  % default language last
]{article}


% === PACKAGES IMPORT / CONFIGURATION =========================================

\usepackage{graphicx}

% --- ENCODING / FONTS -----------------

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{verbatim}
\usepackage{fancyvrb}
% \usepackage{kpfonts}
% \usepackage{microtype}

% --- i18n, l10n -----------------------

\usepackage{babel}
\usepackage[iso]{isodate}

\babeltags{en=english,fr=french}

% --- LAYOUT ---------------------------

\usepackage[%
a4paper,
margin=27.5mm,  % tighter margins
]{geometry}
% \usepackage{pdflscape}  % landscape environment

% --- MATHEMATICS TYPESETTING ----------

% \usepackage{amsmath}
% \usepackage{amssymb}
% \usepackage{amsthm}

% --- GRAPHICS / FIGURES ---------------

% \usepackage{graphicx}
\usepackage{xcolor}
% \usepackage{cbfcolors}

% --- LISTINGS / ALGORITHMS ------------

% \usepackage[%
% 	linesnumbered,
% 	ruled,
% ]{algorithm2e}  % pseudo-code
% \usepackage[cache=false]{minted}  % requires pygmentize

% --- ENHANCED TABLES ------------------

\usepackage{booktabs}  % publication quality tables
% \usepackage{tabularx}  % extended column syntax
% \usepackage{multirow}  % mergeable rows

% --- CITATIONS ------------------------

\usepackage[%
backend=biber,
safeinputenc,
doi=true,
isbn=false,
sorting=none,
maxbibnames=10,
defernumbers=true,
urldate=iso,
seconds=true
]{biblatex}

\DeclareSourcemap{\maps[datatype=bibtex]{\map{\step[fieldset=shorthand,null]}}}  % ignore shorthand field
\renewcommand*{\mkbibnamefamily}[1]{\textsc{#1}}  % capitalize last names
\renewcommand*{\mkbibnameprefix}[1]{\textsc{#1}}  % capitalize prefix

\addbibresource{references.bib}

% --- MISC. PACKAGES -------------------

% \usepackage[inline]{enumitem}  % enhanced enumerations
\usepackage[strict=true]{csquotes}  % advanced quotes
\usepackage[%
binary-units,
per-mode=symbol,
]{siunitx}  % correct units for physical quantities

\usepackage{mdframed}
\mdfdefinestyle{alert}{%
	linewidth=3pt,%
	hidealllines=true,%
	leftline=true,%
	rightline=true,%
}
\newmdenv[%
style=alert,%
linecolor=red,%
backgroundcolor=red!10,%
]{alert-danger}

% --- PDF SUPPORT ----------------------

\usepackage{url}
\usepackage[%
pdfusetitle,
unicode=true,
plainpages=false,
colorlinks=true,
% pdfborder={0 0 0},
breaklinks=true,  % allow line breaks inside links
bookmarksnumbered=true,
bookmarksopen=true,
bookmarksopenlevel=1,
]{hyperref}  % import as last package
\usepackage[capitalise]{cleveref}  % import after hyperref

% extmeta-data (execution is deferred after the meta-data has been set)
\makeatletter
\AtBeginDocument{%
	\hypersetup{%
		pdftitle=\@title,
		pdfauthor=\@author,
	}
}
\makeatother


% === MACROS DEFINITION =======================================================

% --- COMMON ABBREVIATIONS MACROS ------

\newcommand{\cad}{c.-à-d.\@}
\newcommand{\cf}{cf.\@}
\newcommand{\sic}{\emph{sic}}
\newcommand{\versus}{\emph{vs.\@}}

\makeatletter
\newcommand{\etc}{etc\@ifnextchar.{}{.\@}}
\makeatother

% --- SPECIFIC ABBREVIATIONS MACROS ----

\newmdenv[%
style=alert,%
linecolor=red,%
backgroundcolor=red!10,%
]{tmp}


% === META DATA ===============================================================

\title{Rapport de stage de L3 : Composition d'algorithmes d'ordonnancement}  % document title
\author{Elias \textsc{Suvanto}}
% \date{\printdate{2020-05-08}}
\date{\today}


% === DOCUMENT CONTENT ========================================================

\begin{document}
	
	\begin{titlepage}
		\begin{center}
			\vspace*{1cm}
			
			\Huge
			\textbf{Rapport de stage en distanciel de L3}
			
			\vspace{0.5cm}
			\LARGE
			Composition d'algorithmes d'ordonnancement
			
			\vspace{1.5cm}
			
			\textbf{Elias Suvanto} \\
			\Large
			Département Informatique Fondamentale\\
			ENS de Lyon
			
			\vspace{0.8cm}
			
			\LARGE
			Encadré par \textbf{Gr\'egory Mouni\'e} (\'equipe DATAMOVE) et \textbf{Rapha\"el Bleuse} (\'equipe CORSE)\\
			\Large
			IMAG, Grenoble
			
			\vfill
			
%			\includesvg{assets/ens-de-lyon}
%			\includegraphics[width=2.7in]{assets/imag.png}
			
		\end{center}
	\end{titlepage}
	
	
%	Encadre par Gregory Mounié dans l'équipe DATAMOVE au Laboratoire d'Informatique de Grenoble.
	
%	\pagebreak
	
	\begin{abstract}
		
		\begin{tmp}
			De nombreuses variantes de problèmes d'ordonnancement existent. La bibliographie en fournit même une notation \`a 3 champs introduite par Graham et al. \cite{Graham}, le premier pour l'environnement de la machine, le deuxième pour les caractéristiques et contraintes des t\^aches et le dernier pour la fonction objectif \`a minimiser.
			
			Face \`a l'explosion combinatoire des variantes possibles, les implémentations que fournissent les chercheurs pour chacun de ces problèmes sont spécialisées sur une architecture donnée et ne passent pas forc\'ement bien \`a l'échelle. De plus, de nombreux morceaux de code sont identiques d'une implémentation \`a une autre et il est dommage de les réécrire \`a chaque occasion.
			
			De nombreux problèmes d'ordonnancement sont NP-difficiles et par conséquent, les stratégies actuelles sont des heuristiques. Elles peuvent consister \`a découper les t\^aches selon le temps et \`a lancer des heuristiques dans chaque étage.
			
			Ce stage a pour ambition de démontrer la faisabilité d'une librairie permettant de construire efficacement des ordonnanceurs en étendant le modèle d'ordonnanceur modulaire de StarPU.
			
			Le langage d'implémentation choisi est Rust sa gestion de la mémoire.
		\end{tmp}
	\end{abstract}
	
	\tableofcontents
	
	\section{Calcul parallèle et ordonnancement}
	
	\subsection{Origine du problème}
	
	Augmenter la fr\'equence des processeurs a \'et\'e une solution pour accro\^itre la puissance de calcul des ordinateurs. Nous sommes aux limites de cette pratique \`a cause de la dissipation thermique et de contraintes techniques, au profit d'un nouveau paradigme : le calcul parall\`ele. Très utilis\'e dans le calcul \`a haute performance, il consiste \`a d\'ecouper le travail en plusieurs t\^aches ind\'ependantes \`a partager entre diff\'erentes unit\'es de calcul. La manière de les distribuer est l'objet d'\'etude de l'ordonnancement.
	
	L'ordonnancement est la branche de la recherche op\'erationnelle en informatique qui s'occupe d'optimiser certains crit\`eres dans l'assignation de ces t\^aches aux unit\'es de calcul, souvent sous de nombreuses contraintes.
	
	C'est pourquoi les problématiques d'ordonnancement sont très complexes. On cherche \`a optimiser de nombreux critères différents : efficacité, stabilité, consommation d'\'energie\dots De nombreuses variantes de problèmes d'ordonnancement existent, cf. \url{http://schedulingzoo.lip6.fr/}. La majorit\'e d'entre elles sont NP-difficiles.
	Devant la richesse actuelle de la th\'eorie de l'ordonnancement, il peut \^etre frustrant de ne pas r\'eussir \`a reproduire les impl\'ementations des chercheurs et de les \'etendre \`a d'autres architectures.
	Les articles actuels sont souvent très spécialis\'es. Leurs solutions sont implément\'ees la plupart du temps de manière ad hoc, proches du hardware, rendant difficile la reproductibilité des expériences par les lecteurs. Les algorithmes d'ordonnancement simples sont reproductibles et passent bien \`a l'\'echelle mais c'est au prix d'une performance moindre.
	
	Comment alors concilier performance et scalabilité ?
	Le mod\`ele d'ordonnanceur modulaire comme en propose un StarPU est une piste pour faciliter la conception des ordonnanceurs et la réutilisation de composants.
	Utiliser l'ordonnancement modulaire rend son travail reproductible, permet sa réutilisation et passe \` a l'\'echelle. Il reste \`a le rendre aussi performant que des implémentations spécifiques \`a certaines architectures.
	
	
	\subsection{Pourquoi le problème est intéressant}
	
	Trouver un mod\`ele pour construire des ordonnanceurs \`a la fois performants, scalables et reproductibles est difficile vu la diversit\'e des contraintes. N\'eanmoins, en pratique, les t\^aches ne sont pas connues d'avance et arrivent au cours de l'ex\'ecution. L'ordonnancement se fait alors en ligne.
	Comme on peut le lire dans le chapitre Online Scheduling de \cite{IntroductionToScheduling}, il est souvent impossible de calculer la solution optimale pour l'ordonnancement en ligne. Mais cette classe de probl\`emes est celle qui intervient le plus en pratique. On se concentrera sur l'ordonnancement en ligne.
	
	Un des outils th\'eoriques pour \'evaluer th\'eoriquement ces algorithmes en ligne est l'analyse comp\'etitive qui consiste \`a encadrer le r\'esultat d'un algorithme par les r\'esultats d'autres algorithmes mais hors-ligne. Cette analyse peut \^etre difficile \`a effectuer, donc comparer exp\'erimentalement des algorithmes en ligne se fait souvent.
	Pour s'abstraire de l'architecture sur laquelle tourne les algorithmes, on utilise un support d'ex\'ecution comme StarPU. 
	
	\subsection{État de l'art}
	
	L'article de Marc Sergent et Simon Archipoff\cite{sergent:hal-00978364} s'int\'eresse \`a ce probl\`eme en précisant que la litt\'erature n'a pas vraiment pr\'ecis\' ce que devait \^etre la structure d'un ordonnanceur. Ils en proposent un mod\`ele. Il s'agit d'un mod\`ele d'ordonnanceur modulaire, un outil qui permet de construire des ordonnanceurs \`a partir de composants. StarPU en propose un avec une petite biblioth\`eque de composants. Il permet de faciliter la conception des ordonnanceurs et la réutilisation de composants. Le détail du mod\`ele est donn\'e dans le paragraphe 2.2.
	
	\subsection{Plan du rapport}
	
	Ce rapport se compose de 2 volets. Le premier pose la problématique de la modularisation des ordonnanceurs de t\^aches, indique les travaux en cours sur le sujet et en tire une stratégie de composition d'ordonnancement inspir\'ee par le mod\`ele d'ordonnanceur modulaire. Le second volet se concentre sur l'implémentation en Rust d'exemples de composition d'algorithmes d'ordonnancement pour démontrer la faisabilité de l'approche.
	
	\section{Modulariser un ordonnanceur}
	
	\subsection{Que fait un ordonnanceur ?}
	.
	Un ordonnanceur doit distribuer les t\^aches qu'il reçoit aux ressources de la machines en respectant certaines contraintes tout en minimisant une fonction objectif.
	
	Les probl\`emes d'ordonnancement sont souvent d\'ecrits avec une notation \`a 3 champs de la forme $\alpha | \beta | \gamma$ o\`u :
	\begin{enumerate}
		\item $\alpha$ d\'esigne l'
		environnement de la machine, \`a savoir le nombre d'unit\'es de calcul, s'ils sont identiques, ont une relation, sont disparates, ...
		\item $\beta$ indique les contraintes qui peuvent \^etre des contraintes de pr\'ec\'edence qui se mod\'elisent par un DAG (graphe orient\'e acyclique), un arc indiquant que la t\^ache source doit \^etre termin\'ee avant que la t\^ache destination d\'ebute. On peut aussi pr\'eciser les temps de calcul des t\^aches, les  \'eventuelles deadlines, ...
		\item $\gamma$ indique la fonction \`a optimiser qui peut \^etre la somme des temps de calcul, le makespan (dur\'ee dans laquelle sont effectu\'ees toutes les t\^aches), le retard maximal d'un t\^ache par rapport \`a une deadline,... Il peut aussi y avoir des pond\'erations.
	\end{enumerate}
	
	Les t\^aches qui rentrent dans l'ordonnanceur doivent atterrir un peu plus tard sur une ressource. Elles suivent un chemin de décision dans l'ordonnanceur et peuvent se reposer dans des réservoirs en cas d'attente.
	
	\subsection{Modèle d'ordonnanceur modulaire de StarPU \cite{sergent:hal-00978364}}
	
	Marc Sergent et Simon Archipoff ont déjà présenté un premier modèle d'ordonnanceur modulaire \cite{sergent:hal-00978364} bas\'e sur la librairie de StarPU. Le plus complexe est la d\'efinition des interfaces de communication entre les composants. L'encapsulation des donn\'ees est une bonne pratique pour permettre la red\'efinition du comportement des composants en modifiant leurs m\'ethodes.
	
	4 classes de composants sont \`a distinguer : Réservoir, Aiguilleur, composant \`a effet de bord et Worker StarPU. L'interface de communication se d\'efinit par 4 méthodes : Push, Pull, CanPush, CanPull. Les t\^aches arrivent dans la structure arborescente de l'ordonnanceur par le r\'eservoir racine qui reçoit les tâches Les t\^aches sortent de l'ordonnanceur par les feuilles qui sont les workers. Cet arbre est biparti avec d'un c\^ot\'e les r\'eservoirs, de l'autre les autres composants. Cela permet aux t\^aches d’être stock\'ees si elles ne peuvent plus progresser vers les workers.
	
	
	\section{Un premier prototype de composition d'ordonnancement}
	
	Je me suis inspir\'e du modèle de StarPU pour impl\'ementer un simulateur d'ordonnanceur modulaire en Rust. Mais je n'ai pas repris exactement la m\^eme interface de communication.
	
	Dans un tel ordonnanceur, les t\^aches ne progressent pas toutes seules. 
	
%	Conjecture : entasser les t\^aches dans les r\'eservoirs des workers n'est pas la meilleure id\'ee.

	
%	Arg : Si le worker est inactif et qu'une tache dont il est le meilleur spécialiste se trouve en amont, c'est optimal de la prendre. Sinon. elle sera attribuée \`a un worker moins sp\'ecialiste et plus occup\'e.
	Comme les t\^aches arrivent \`a des dates inconnues, je les inscris en tant qu'\'ev\'enement NewTask. Par ailleurs, les workers ne doivent pas \^etre paresseux. Ils doivent r\'eclamer des t\^aches lorsqu'ils sont inactifs, d'o\`u le signal TryPull.
	Donner tout de suite une t\^ache \`a un worker inactif n'est pas forc\'ement optimal puisqu'un worker actif beaucoup plus efficace pour traiter cette t\^ache pourrait se lib\'erer dans un instant.
	C'est aux aiguilleurs de d\'ecider \`a qui donner une t\^ache. Un signal montant CanPush permet de leur donner cette mission.
	Lorsqu'un composant r\'eceptionne une t\^ache, il doit s'en d\'ebarrasser, d'o\`u le signal TryPush. Chaque signal peut en engendrer d'autres.
	
	\begin{enumerate}
		\item \textbf{NewTask} : Signal indiquant au composant racine qu'une nouvelle t\^ache est disponible. Elle est immédiatement stockée dans le réservoir racine de capacité "infinie".
		\item \textbf{TryPush} : Quand un composant reçoit ce signal, il essaie de vider une t\^ache de son réservoir \`a ses enfants. En cas de réussite, il se renvoie un signal TryPush et il en envoie un \`a l'enfant qui vient de recevoir une t\^ache.
		\item \textbf{CanPush} : Signal issu d'un composant dont le réservoir est vide. Ce signal remonte jusqu’à tomber sur un composant dont le réservoir n'est pas vide. Il se transforme alors en signal TryPush.
		\item \textbf{TryPull} : Le signal TryPull appara\^it pour tout worker qui devient inactif, ou si le composant parent vient de recevoir des t\^aches. S'il a encore des t\^aches dans son r\'eservoir personnel, il en pioche une. Sinon son r\'eservoir est vide. Il engendre alors un signal CanPush.
		
%		Un Worker reçoit ce signal s'il vient de finir un travail ou si on essaie de lui faire passer des t\^aches et qu'il est disponible. Il cherche une t\^ache dans son réservoir. S'il y arrive, il retient qu'il faudra effectuer un autre TryPull lorsqu'il aura fini son travail.
	\end{enumerate}

	Je n'ai pas démontré qu'aucune t\^ache ne peut rester coinc\'e quelque part dans le cas g\'en\'eral mais c'est le cas pour les ordonnanceurs que j'ai faits. 
	On peut associer \`a la file de priorit\'e une quantit\'e positive strictement d\'ecroissante \`a la transformation de chaque signal, ce qui assure que le simulateur se termine.
%	Le nombre de signaux finit toujours par arriver \`a 0. Seul le signal TryPush peut augmenter le nombre de signaux.
%	Lorsque le signal TryPush augmente le nombre de signaux de 1, c'est qu'une t\^ache vient de descendre.
%	Le nombre de signaux est major\'e par le nombre de descentes de t\^aches possibles, qui est d\'ecroissant. . De plus, les autres signaux n'augmentent pas le nombre de signaux et s’éteignent \`a un moment (CanPush est montant, TryPull ne dure pas)
	\section{Implémentation en Rust}
	
	Rust \`a été un nouveau langage pour moi \`a apprendre.
	J'ai cod\'e un algorithme classique de List-Scheduling et un simulateur d'ordonnanceur modulaire (cf. dépôt Git).

	
	\subsection{Borrowing et gestion de la mémoire en Rust}
	
	\subsubsection{Notion de propri\'et\'e}
	Pour comprendre le choix d'implémentation de la structure arborescente des composants en Rust, il faut rappeler les contraintes imposées par Rust en matière de pointeur. En effet, Rust n'a pas de ramasse-miette tel que OCaml. Il n'exige pas non plus l'allocation explicite de la mémoire comme en C. Pour gérer la mémoire le plus efficacement possible, Rust utilise la notion de propriété. Concrètement, le compilateur exige que le  programmeur respecte les 3 règles suivantes :
	
	\begin{itemize}
		\item Chaque valeur a un propriétaire. 
		\item \`A un instant donn\'e, il ne peut y avoir qu'un seul propriétaire d'une valeur.
		\item Lorsque le propriétaire n'est plus dans la portée, la valeur est libérée.
	\end{itemize}
	
	Ces règles permettent d'assurer que les r\'ef\'erences sont toujours valides en Rust. Elles permettent d’éviter les memory leaks et les segfaults, des erreurs ayant lieu \`a l’exécution. Rust est exigeant \`a la compilation mais garantit une exécution fiable.
	
	Voici un exemple de la documentation \`a ce sujet \url{https://doc.rust-lang.org/1.8.0/book/ownership.html} :
	\begin{Verbatim}
	let v = vec![1, 2, 3];
	let v2 = v;
	println!("v[0] is: {}", v[0]);
	\end{Verbatim}
		
	Ce code choque le compilateur de Rust puisqu'une ressource se voit attribuer 2 noms, ce qui est contraire aux règles de propri\'et\'e. Le compilateur affiche :
	\begin{Verbatim}
	error: use of moved value: `v`
	println!("v[0] is: {}", v[0]);
	\end{Verbatim}
	\subsubsection{Notion d'emprunt}
	
	Lors d'un appel de fonction, la fonction devient momentanément propri\'etaire des arguments et elle doit alors les retourner \`a l'appelant pour qu'il puisse continuer \`a les utiliser.
	Néanmoins, Rust permet un emprunt plus automatique en passant par les r\'ef\'erences. Lorsque l'appel prend fin, l'objet est automatiquement rendu \`a l'emprunteur.
	Pour que la fonction puisse modifier l'objet pass\'e en argument, elle doit prendre une r\'ef\'erence mutable.
	Rust autorise le pr\^et multiple de r\'ef\'erences non mutables, le pr\^et unique de r\'ef\'erence mutable mais jamais les 2 \`a la fois. Cela permet d'\'eviter que 2 pointeurs soient simultan\'ement en lecture et en \'ecriture sur une m\^eme zone m\'emoire. Il n'y a ainsi pas de situation de comp\'etition possible en Rust.
	
	Voici un exemple de la documentation \`a ce sujet \url{https://doc.rust-lang.org/1.8.0/book/references-and-borrowing.html} :
	\begin{Verbatim}
	let mut x = 5;
	let y = &mut x;
	*y += 1;
	println!("{}", x)};
	\end{Verbatim}
	
	Il y a \`a la fois un emprunt mutable et non mutable ce que interdit le compilateur de Rust qui affiche :
	\begin{Verbatim}
	error: cannot borrow `x` as immutable because it is also borrowed as mutable
	println!("{}", x);
	\end{Verbatim}
	
	\subsubsection{Notion de temps de vie}
	
	Rust emp\^eche toute situation o\`u la r\'ef\'erence \`a une ressource qui est valide plus longtemps que la ressource en question gr\^ace aux temps de vie des r\'ef\'erences. Souvent d\'eduite implicitement, Rust demande en cas d'ambiguïté qu'on la d\'etaille.
	
	\subsection{Stratégie adoptée pour implémenter l'arbre de composants}
	
	Tenter d’implémenter un arbre bidirectionnel avec des pointeurs, c'est se confronter au borrow-checker \`a la compilation. 3 pistes sont envisageables :
	\begin{itemize}
		\item La première est d'utiliser le mot-clef unsafe pour passer outre le borrow-checker mais comme l'indique le nom, c'est \`a éviter.
		\item La deuxième est d'utiliser la librairie RefCell permettant de reporter les règles de propriété de Rust vérifiées \`a la compilation \`a l’exécution, de rendre mutable des r\'ef\'erences qui sont partag\'ees.
		\item La troisième est de gérer soi-même la mémoire avec des méthodes bas-niveau sur des tableaux.
	\end{itemize}
	

	J'ai choisi la troisième option pour ne pas alourdir la syntaxe avec la deuxième.
	
	J'ai d\'efini les structures de donn\'ees pour les objets manipul\'es (type $\mathbf{enum}$ pour les composants, politiques d'ordonnancement, type de travailleur, $\mathbf{struct}$ pour les t\^aches et les nœuds de l'arbre)
	Par exemple, le code suivant d\'efinit le type d'\'ev\'enement avec un signal.
	\begin{Verbatim}
	#[derive(Debug, PartialEq, Hash)]
	enum EventType {
		NewTask(Task),
		TryPull(usize),
		TryPush(usize),
		CanPush(usize),
	}
	\end{Verbatim}
	
	J'utilise la file de priorit\'e de Rust pour simuler les \'ev\'enements pas \`a pas avec les dates. Le corps du code se pr\'esente alors ainsi :
	
	\begin{Verbatim}
	while !events.is_empty() {
		let event = events.pop();
		match event {
			None => (),
			Some((EventType::NewTask(task), date)) => ...,
			Some((EventType::TryPull(idworker), date)) => ...,
			Some((EventType::TryPush(idnode), date)) => ...,
			Some((EventType::CanPush(idnode), date)) => ...,
		}
	}
	\end{Verbatim}
	Le signal est assorti de l'identifiant du nœud pour qu'on puisse y acc\'eder. Le composant qui r\'eceptionne un signal fait \'eventuellement circuler les t\^aches et cr\'ee d'autres t\^aches.
	Le signal TryPush est interprété différemment selon les composants. En particulier, un aiguilleur pouvant avoir plusieurs enfants, il choisira l'enfant \`a qui envoyer la t\^ache selon sa politique d'ordonnancement. Il peut consulter le chargement du composant enfant.
	Le code apr\`es chaque cas suit la description faite des signaux.
	\section{Validation de l'approche}
	
	\subsection{En quoi l'ordonnanceur modulaire est un modèle générique pour l'ordonnancement}
	
	\subsubsection{Deux ordonnanceurs modulaires simples}
		Le simulateur d'ordonnanceur modulaire que j'ai cod\'e m'a permis de comparer 2 exemples d'ordonnanceurs simples. On considère des t\^aches séquentielles avec un certain temps de calcul sur CPU et un autre sur GPU.
		Le 1er exemple se constitue juste d'un aiguilleur qui distribue la t\^ache de manière \`a minimiser le temps de fin de calcul avec un algorithme de List-Scheduling. Le 2\`eme exemple distribue les t\^aches au CPU ou au GPU selon leur temps de calcul prévu.
	
	\begin{figure}[!h]
		\centering
		\includegraphics[width=1\linewidth]{img/composing_example2}
		\caption{}
		\label{fig:composingexample2}
	\end{figure}
	
	
	\begin{figure}[!h]
		\centering
		\includegraphics[width=1\linewidth]{img/composing_example1}
		\caption{}
		\label{fig:composingexample1}
	\end{figure}

	\begin{figure}[!h]
		\centering
		\includegraphics[width=0.2\linewidth]{img/legende}
		\caption{}
		\label{fig:legende}
	\end{figure}
	
	Les fl\`eches indiquent la direction dans laquelle se d\'eplacent les t\^aches. Les signaux, eux, sont bidirectionnels.
	Les rectangles blancs d\'esignent les workers et sont les feuilles de l'arbre. C'est ici que peuvent \^etre effectu\'ees les t\^aches. Les aiguilleurs (Eager) sont indiqu\'es en losange jaune. Ils impl\'ementent la politique de l'ordonnancement et peuvent avoir plusieurs enfants. Les r\'eservoirs en cylindre bleu stockent les t\^aches en cours d'acheminement. Les aiguilleurs de ces 2 ordonnanceurs suivent la politique suivante : pousser une t\^ache de manière \`a ce qu'elle soit termin\'ee le plus t\^ot possible. Les aiguilleurs peuvent consulter la date prochaine de disponibilit\'e de leurs enfants.
	
	
	\subsubsection{Exemple illustratif}
	
	Après avoir lanc\'e les 2 ordonnanceurs sur nombreux tests, le makespan et la surface de calcul sont similaires sauf lorsque les réservoirs sont de capacité 1. Il y a peut-être un problème avec l'implémentation ou le modèle.
	La bonne nouvelle, c'est que le programme s'est toujours arrêté et sur des petits exemples, m'affichait des assignations correctes. 
	
	Par exemple, supposons la configuration soit composée de 2 cœurs CPU, 2 cœurs GPU et que les r\'eservoirs du mod\`ele soient de capacité 2.
	On demande \`a nos exemples d'ordonnanceurs modulaires d'ordonnancer les 12 t\^aches suivantes :\\
	
	\begin{center}
		\begin{tabular}{|c|c|c|c|}
			\hline
			id & date d'arrivée & temps de calcul CPU & temps de calcul GPU \\
			\hline
			0 & 2 & 6 & 16 \\
			\hline
			1 & 3 & 5 & 16 \\
			\hline
			2 & 4 & 6 & 1 \\
			\hline
			3 & 1 & 7 & 10 \\
			\hline
			4 & 3 & 1 & 10 \\
			\hline
			5 & 2 & 12 & 18 \\
			\hline
			6 & 0 & 8 & 1 \\
			\hline
			7 & 1 & 2 & 14 \\
			\hline
			8 & 2 & 10 & 7 \\
			\hline
			9 & 4 & 15 & 4 \\
			\hline
			10 & 5 & 1 & 9 \\
			\hline
			11 & 1 & 2 & 4 \\
			\hline
		\end{tabular}	\\
	\end{center}
	On peut afficher le r\'esultat des deux ordonnanceurs sur cette instance.
	
	\begin{figure}[!h]
		\centering
		\includegraphics[width=1\linewidth]{img/withoutpartitioner}
		\caption{}
		\label{fig:result1}
	\end{figure}
	
	Les t\^aches sont repr\'esent\'ees par des rectangles num\'erot\'es de largeur correspondante \`a leur dur\'ee d'ex\'ecution. La partie sup\'erieure correspond aux 2 cœurs CPU, l'inf\'erieure aux 2 cœurs GPU.
	
	\begin{figure}[!h]
		\centering
		\includegraphics[width=1\linewidth]{img/withpartitioner}
		\caption{}
		\label{fig:result2}
	\end{figure}
	
	Le partitionneur permet d'économiser du calcul puisqu'il distribue la t\^ache de pr\'ef\'erence aux unit\'es de calcul qui vont l'ex\'ecuter le plus rapidement \`a moins que ce ne soit pas possible. On remarque que l'ordonnanceur avec le partitionneur a pr\'ef\'er\'e donner la t\^ache 1 aux CPU (elle dure alors 5 temps) au lieu du GPU (pour lequel elle dure 16).
	
		
	\subsubsection{G\'en\'eration de tests}
	J'ai compar\'e les r\'esultats des 2 ordonnanceurs sur une banque de tests al\'eatoire dont la g\'en\'eration est d\'etaill\'ee, dans l'optique de valider le comportement des 2 ordonnanceurs.
	
	Ces ordonnanceurs ne sont pas des solutions r\'evolutionnaires sur le probl\`eme d'ordonnancement qu'on s'est pos\'e mais sont des exemples simples qui d\'emontrent l'int\'er\^et du mod\`ele. Avec une diff\'erence mineure de construction et simple \`a mettre en place, montrons qu'on peut am\'eliorer les r\'esultats. 
	
	Il y a plusieurs dimensions : 
	\begin{itemize}
		\item Le nombre de t\^aches $n$.
		\item La date maximale $T$ \`a laquelle peuvent arriver les t\^aches.
		\item Le nombre de cœurs $C$ de CPU et de GPU. 
		\item La dur\'ee maximale d'une t\^ache $D$.
		\item La capacit\'e des r\'eservoirs $r$
	\end{itemize}
	
	Fixons $T = 4^7$, $C = 4^3$, $D = 4^5$.
	Je note $x$ un facteur de densit\'e pris selon la loi uniforme entre 1 et 100.
	Je note $n(x) = 4^6 \times x$.
	
	On prend al\'eatoirement une capacit\'e qui vaut $1, 5$ ou 30.
	On g\'en\`ere $n$ t\^aches de la forme suivante :
	\[\text{arrivaldate cputime gputime}\]
	
	les 3 param\`etres sont pris al\'eatoirement uniform\'ement entre 1 et T, entre 1 et D et entre 1 et D.	
	
	J'ai ex\'ecut\'e 514 fois l'exp\'erience al\'eatoire. L'espérance du nombre de t\^aches est environ de 2 millions.
	
	\begin{figure}[!h]
		\centering
		\includegraphics[width=1.1\linewidth]{img/surfacevsdensity2}
		\caption{}
		\label{fig:surfacevsdensity2}
	\end{figure}
	
	En abscisse, on a la pression sur l'ordonnanceur. En ordonn\'ee, on peut lire le gain de surface de calcul obtenu gr\^ace au partitionneur. On voit que pour une pression de plus de 1000 sur l'ordonnanceur, on a un gain de 26\% sur l'ordonnanceur sans partitionneur. On voit \'egalement que ce gain est beaucoup plus faible avec des r\'eservoirs de capacit\'e 1.
	
	En effet, le partitionneur, face au signal TryPush, s'il ne peut pas pousser la t\^ache vers la zone la plus efficace parce qu'elle est d\'ej\`a pleine, va l'envoyer dans l'autre zone. Cela permet de garder un bon makespan, en gardant un \'equilibre entre CPU et GPU. Avec des petits r\'eservoirs, le partitionneur ne trie plus aussi bien car les zones en aval sont pleines.
	
	Remarquons que la surface de calcul o\`u peuvent arriver les t\^aches vaut $T \times C = 4 ^{11}$ et que la surface de calcul esp\'er\'ee est $n \times D / 2 = 4^{11} \times x$. Plus x est grand, plus il y a pression sur l'ordonnanceur. Il reçoit rapidement la totalit\'e des t\^aches.
	
	Essayons de voir pourquoi on ne peut pas esp\'erer un gain de surface de calcul de plus de 33\%.
	En effet, soit $t_1$ et $t_2$ deux variables al\'eatoires r\'eelles uniformes ind\'ependantes entre 0 et 1. Posons $S_1 = min(t_1,t_2)$ le temps de calcul d'un t\^ache qu'on peut esp\'erer et $S_2 = \frac{t_1 + t_2}{2}$ si la t\^ache va indiff\'eremment vers les CPU au GPU, on calcule l'esp\'erance : 
	\[\mathbb{E}(min(t_1,t_2)) = \int_{0}^{1} \mathbb{P}(min(t_1,t_2) > t) dt\]
	
	Or par indépendance, $\mathbb{P}(min(t_1,t_2) > t) = \mathbb{P}(t_1 > t, t_2 > t) = \mathbb{P}(t_1 > t)\mathbb{P}(t_2 > t) = (1-t)^2$.
	Finalement,
	\[\mathbb{E}(S_1) = \frac{1}{3}\]
	Alors que
	\[\mathbb{E}(S_2) = \frac{1}{2}\]
	Donc
	\[\frac{\mathbb{E}(S_1)}{\mathbb{E}(S_2)} = \frac{2}{3}\]
	
	On peut s'attendre \`a ce que $S_1$ soit le temps de calcul d'une t\^ache quand on a le partitionneur. Dans l'ordonnanceur sans partitionneur, $S_2$ mod\'elise moins bien le temps de calcul de la t\^ache puisque la t\^ache n'est pas vraiment distribu\'ee au hasard entre CPU et GPU. On pourrait faire un autre ordonnanceur avec un composant Random qui distribue les t\^aches au hasard.
	
	L'ordonnanceur avec partitionneur permet donc \'economiser du calcul et donc de l'\'energie.
	De plus, cela n'impacte visiblement pas le makespan de ce test.
	
	\begin{figure}[!h]
		\centering
		\includegraphics[width=0.5\linewidth]{img/makespanSurface}
		\caption{}
		\label{fig:makespansurface}
	\end{figure}
	
	On a une relation quasi-lin\'eaire entre les ratios de makespan et de surface de calcul.\\\\
	 
	\begin{figure}[!h]
		\centering
		\includegraphics[width=0.8\linewidth]{img/surfacevsdensity4}
		\caption{}
		\label{fig:surfacevsdensity4}
	\end{figure}
	
	Avec moins de cœurs et de t\^aches, la figure 8 montre que le gain de surface est pr\'esent mais la relation avec le makespan n'est plus si vraie.
	
	\begin{figure}[!h]
		\centering
		\includegraphics[width=0.8\linewidth]{img/makespanSurface4}
		\caption{}
		\label{fig:makespansurface4}
	\end{figure}
	
	Ainsi, l'ajout d'un partitionneur permet d'\'economiser des calculs sur les entr\'ees o\`u la quantit\'e de travail arrive rapidement en masse.
	
	\section{Conclusion}
	
	\subsection{Résultats et difficult\'es}
	
	Gr\^ace au simulateur d'ordonnanceur modulaire, j'ai pu comparer 2 ordonnanceurs modulaires simples au niveau du makespan et du calcul total. On r\'epond aux exigences de scalabilit\'e et de flexibilit\'e par rapport \`a l'architecture gr\^ace aux composants.
	
	\'Ecrire en Rust pour de la programmation orient\'ee objet a \'et\'e difficile \`a cause des exigences de Rust en mati\`ere de pointeurs. Mais le temps pass\'e \`a les respecter m'a permis de n'avoir quasiment rien \`a d\'ebugguer.
	
	
	\subsection{Perspectives}
	Il est tr\`es simple d'assembler les composants et c'est facile d'en impl\'ementer de nouveaux. On pourrait r\'eduire la syntaxe des constructions d'ordonnanceur en utilisant des macros Rust, voire \'ecrire un DSL comme Bossa\cite{Bossa}.
	\printbibliography
	
	\appendix
	
	\section{Contexte institutionnel}
	
	Mon stage s'est fait majoritairement \`a distance, avec une journ\'ee de visite au laboratoire IMAG sur le campus universitaire de Grenoble dont je remercie mes encadrants. J'ai eu l'occasion de rencontrer des chercheurs des \'equipes DATAMOVE et POLARIS durant cette journ\'ee et poser quelques questions. 
	
	\section{Dépôt Git}
	
	\url{https://gitlab.com/E_lex/journal.org}
	
	
\end{document}