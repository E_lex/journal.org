//takes the number of tasks n, the number of processors m
//the execution time p[i] of each processor i
//the number of processors r[k] needed by each task k
//returns a schedule (makespan but we could also return the beginning time and processors used by each task)
//After assigning a task, we update the availability date of each processor and sort them
//So the complexity of this list parallel tasks schedulng algoirithm is O(n m log m)
fn parallel_task_scheduling(n: i32, m: i32, p: &[i32], r: &[usize]) -> i32 {
    //We keep in memory the next available date and the id of each processor
    //We want it sorted at every moment 
    let mut available_date_proc: Vec<(i32, usize)> = Vec::new();
    let mut id:usize = 0usize;
    //Each processor is available at time 0 at the beginnning
    for _ in 0..m {
        available_date_proc.push((0, id));
        id = id + 1usize;
    }
    //We keep in memory the tasks that we have already assigned
    let mut removed: Vec<bool> = Vec::new();
    for _ in 0..n {
        removed.push(false);
    }
    //We assign n times a task
    for _ in 0..n {
        //We choose the task to assign
        //To do this, we take the minimum possible date 
        let mut dmin = i32::MAX; //initialized to infinity
        let mut task_to_assign = 0;
        for (i, rt) in r.iter().enumerate() {
            if !removed[i] {
                //maximum of the first ri smallest possible date of processors
                let possible_beginning_date = available_date_proc[*rt].0;

                if possible_beginning_date < dmin {
                    dmin = possible_beginning_date;
                    task_to_assign = i;
                }
            }
        }
        //We assign this task
        let new_date:i32 = p[task_to_assign] + dmin;
        //println!("Assigned task = {}, p = {}", task_to_assign, p[task_to_assign]);
        //Update the available date of each processor
        update(&mut available_date_proc, r[task_to_assign], new_date);
        removed[task_to_assign] = true;
        
        /*println!("\n assigned task {}", task_to_assign);
        for processor in &available_date_proc {
                println!("processor {}, availabiliy date = {}", processor.1, processor.0);
        }*/
    }
    //calculate the makespan
    calculate_makespan(available_date_proc)
}

//updates the list of available time of processors by assigning a task using rt processors
fn update(available_date_proc: &mut Vec<(i32, usize)>, rt: usize, new_date: i32) -> () {
    for i in 0..(rt+1) {
        available_date_proc[i].0 = new_date;
    }
    available_date_proc.sort();
}

fn calculate_makespan(available_time: Vec<(i32, usize)>) -> i32 {
    let mut dmax = 0;
    for processor in &available_time {
        if processor.0 > dmax {
            dmax = processor.0;
        }
    }
    dmax
}

fn main() {
    let n = 5;
    let m = 2;
    let p = [1, 2, 3, 4, 5];
    let r = [1, 0, 1, 1, 0];
    println!("Makespan = {}", parallel_task_scheduling(n, m, &p, &r))
}