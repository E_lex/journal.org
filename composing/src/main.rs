extern crate sdl2; 

use sdl2::pixels::Color;
use std::time::Duration;
use sdl2::rect::Rect;

struct Assignment {
    number_of_tasks:  i32,
    tasks: Vec<i32>,
    beginning_dates: Vec<i32>,
    used_processors: Vec<Vec<i32>>,
    makespan: i32,
}

//takes the number of tasks n, the number of processors m
//the execution time p[i] of each processor i
//the number of processors r[k] needed by each task k
//returns a schedule (beginning time and processors used by each task)
//After assigning a task, we update the availability date of each processor and sort them
//So the complexity of this list parallel tasks schedulng algoirithm is O(n m log m)
fn parallel_task_scheduling(n: i32, m: i32, p: &[i32], r: &[usize]) -> Assignment {
    //We keep in memory the next available date and the id of each processor
    //We want it sorted at every moment 
    let mut available_date_proc: Vec<(i32, usize)> = Vec::new();
    let mut id:usize = 0usize;
    //Each processor is available at time 0 at the beginnning
    for _ in 0..m {
        available_date_proc.push((0, id));
        id = id + 1usize;
    }
    //We keep in memory the tasks that we have already assigned
    let mut removed: Vec<bool> = Vec::new();
    for _ in 0..n {
        removed.push(false);
    }
    let mut tasks: Vec<i32> = Vec::new();
    let mut beginning_dates: Vec<i32> = Vec::new();
    let mut used_processors: Vec<Vec<i32>> = Vec::new();
    //We assign n times a task
    for _ in 0..n {
        //We choose the task to assign
        //To do this, we take the minimum possible date 
        let mut dmin = i32::MAX; //initialized to infinity
        let mut task_to_assign = 0;
        for (i, rt) in r.iter().enumerate() {
            if !removed[i] {
                //maximum of the first ri smallest possible date of processors
                let possible_beginning_date = available_date_proc[*rt-1].0;

                if possible_beginning_date < dmin {
                    dmin = possible_beginning_date;
                    task_to_assign = i;
                }
            }
        }
        tasks.push(task_to_assign as i32);
        //We assign this task
        let new_date:i32 = p[task_to_assign] + dmin;
        beginning_dates.push(dmin);

        let mut used_proc_by: Vec<i32> = Vec::new();
        for i in 0..r[task_to_assign] {
            used_proc_by.push(available_date_proc[i].1 as i32);
        }
        used_processors.push(used_proc_by);
        //Update the available date of each processor
        update(&mut available_date_proc, r[task_to_assign], new_date);
        removed[task_to_assign] = true;
    }
    Assignment {
        number_of_tasks: n,
        tasks: tasks,
        beginning_dates: beginning_dates,
        used_processors: used_processors,
        makespan: calculate_makespan(available_date_proc),
    }
}

//updates the list of available time of processors by assigning a task using rt processors
fn update(available_date_proc: &mut Vec<(i32, usize)>, rt: usize, new_date: i32) -> () {
    for i in 0..rt {
        available_date_proc[i].0 = new_date;
    }
    available_date_proc.sort();
}

fn calculate_makespan(available_time: Vec<(i32, usize)>) -> i32 {
    let mut dmax = 0;
    for processor in &available_time {
        if processor.0 > dmax {
            dmax = processor.0;
        }
    }
    dmax
}


fn display_assignment(a: Assignment, p: &[i32]) -> () {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();
 
    let window = video_subsystem.window("rust-sdl2 demo", (a.makespan*16) as u32, 400)
        .position_centered()
        .build()
        .unwrap();
 
    let mut canvas = window.into_canvas().build().unwrap();

    for i in 0..a.number_of_tasks {
        let task = a.tasks[i as usize] as u32;
        let date = a.beginning_dates[i as usize];
        for ii in (a.used_processors[i as usize]).iter() {
            display_task(task, date, p[task as usize], *ii, &mut canvas);
        }
    }

    canvas.present();
    ::std::thread::sleep(Duration::new(10, 0));
}


//display the assignment of one task
fn display_task(id: u32, date: i32, length: i32, proc: i32, canvas: &mut sdl2::render::Canvas<sdl2::video::Window>) -> () {
    let scale = 15 as i32;
    let scalem = scale-2 as i32;
    let marge: i32 = scalem;
    let x = marge + scale * date;
    let y = marge + scale * proc;
    let dx = (scale * (length-1) + scalem) as u32;
    let dy = scalem as u32;
    let r = (10+80*id) as u8;
    let g = (20+40*id) as u8;
    let b = 150 as u8;
    canvas.set_draw_color(Color::RGB(r, g, b));
    canvas.fill_rect(Rect::new(x, y, dx, dy));
}

fn main() {
    let n = 20;
    let m = 7;
    let p = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    let r = [2, 5, 1, 4, 2, 3, 2, 3, 2, 1, 7, 6, 5, 5, 5, 4, 3, 2, 1, 3];
    let a = parallel_task_scheduling(n, m, &p, &r);
    display_assignment(a, &p);

    //println!("Makespan = {}", parallel_task_scheduling(n, m, &p, &r))
}