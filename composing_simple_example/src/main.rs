use priority_queue::PriorityQueue;
use std::io;

//-----------------Arena Tree-----------
struct Node<T>
{
    idx: usize,
    val: T,
    parent: Option<usize>,
    children: Vec<usize>,
    reservoir_content: Vec<Task>,
    estimated_availability_time: i32,
    capacity: i32,
}

impl<T> Node<T>
{
    fn new(idx: usize, val: T) -> Self {
        Self {
            idx,
            val,
            parent: None,
            children: vec![],
            reservoir_content: vec![],
            estimated_availability_time: 0,
            capacity: 2, //arbitrary value
        }
    }
}

#[derive(Default)]
struct ArenaTree<T>
{
    arena: Vec<Node<T>>,
}

impl<T> ArenaTree<T>
{
    fn node(&mut self, val: T) -> usize {
        // Otherwise, add new node
        let idx = self.arena.len();
        self.arena.push(Node::new(idx, val));
        idx
    }
    
}
//-----------------------------------------------------------------

#[derive(PartialEq, Hash, Debug, Copy, Clone)]
enum WorkerType {
    CPUWorker,
    GPUWorker,
}

#[derive(PartialEq, Hash, Debug, Copy, Clone)]
struct Task { idtask: i32, processing_time_cpu : i32, processing_time_gpu : i32}

//Evenement
#[derive(Debug, PartialEq, Hash)]
enum EventType {
    NewTask(Task),
    TryPull(usize),
    TryPush(usize),
    CanPush(usize),
}

#[derive(Debug)]
enum SchedulingPolicy {
    ListScheduling,
    eager,
}

#[derive(Debug)]
enum Component {
    Worker (WorkerType, i32),
    Broker (SchedulingPolicy),
}

impl Eq for EventType {}

fn read_line() -> String {
    let mut line = String::new();
    io::stdin()
        .read_line(&mut line)
        .expect("Failed to read line");
    line.trim().to_string()
}

fn read_event(idt:i32) -> (EventType, i32) {
    let line = read_line();
    let words: Vec<&str> = line.split_whitespace().collect();
    let time_arrival = words[0].parse().unwrap();
    let task = Task {
        idtask: idt,
        processing_time_cpu: words[1].parse().unwrap(),
        processing_time_gpu: words[2].parse().unwrap(),
    };
    (EventType::NewTask(task), time_arrival)
}

fn main() {  
    let number_tasks: i32 = read_line().parse().unwrap();
    let capacity: i32 = read_line().parse().unwrap();
    let number_cpuworkers: i32 = read_line().parse().unwrap();
    let number_gpuworkers: i32 = read_line().parse().unwrap();

    let mut tree: ArenaTree<Component> = ArenaTree { arena: Vec:: new()};

    let eager = tree.node(Component::Broker(SchedulingPolicy::ListScheduling));
    tree.arena[eager].capacity = i32::MAX;

    let mut cpuworkers: Vec<usize> = Vec::new();
    for _ in 0..number_cpuworkers {
        let cpuworker = tree.node(Component::Worker(WorkerType::CPUWorker, 0));
        tree.arena[eager].children.push(cpuworker);
        tree.arena[cpuworker].parent = Some(eager);
        cpuworkers.push(cpuworker);
    }
    let mut gpuworkers: Vec<usize> = Vec::new();
    for _ in 0..number_gpuworkers {
        let gpuworker = tree.node(Component::Worker(WorkerType::GPUWorker, 0));
        tree.arena[eager].children.push(gpuworker);
        tree.arena[gpuworker].parent = Some(eager);
        gpuworkers.push(gpuworker);
    }


    //Simulation
    let mut events: PriorityQueue<EventType, i32> = PriorityQueue::new();

    for idt in 0..number_tasks {
        let (event, time) = read_event(idt);
        //println!("Event {:#?} {:#?} ", event, time);
        events.push(event, -time);
    }

    //println!("{:#?} ", number_tasks);
    //println!("{:#?} ", capacity);
    //println!("{:#?} ", number_cpuworkers);
    //println!("{:#?} ", number_gpuworkers);
    let mut time = 0;
    let mut surface = 0;
    let mut makespan = 0;
    //Simulateur evenements discrets
    while !events.is_empty() {
        let event = events.pop();
        //println!("Event {:#?} ", event);
        match event {
            None => (),
            Some((EventType::NewTask(task), date)) => 
            {
                time = -date;
                tree.arena[0].reservoir_content.push(task);
                tree.arena[0].estimated_availability_time += task.processing_time_cpu;
                events.push(EventType::TryPush(0), -time);
            },
            Some((EventType::TryPull(idworker), date)) => 
            {
                time = -date;
                match tree.arena[idworker].reservoir_content.pop() {
                    None => {
                        match tree.arena[idworker].parent {
                            None => None,//panic
                            Some(idparent) => events.push(EventType::CanPush(idparent), -time),
                        };
                    },
                    Some(task) => {
                        let processing_time =
                        match tree.arena[idworker].val {
                            Component::Broker(_) => -1, // panic,
                            Component::Worker(WorkerType::CPUWorker, _) => { tree.arena[idworker].val = Component::Worker(WorkerType::CPUWorker, time + task.processing_time_cpu); task.processing_time_cpu},
                            Component::Worker(WorkerType::GPUWorker, _) => { tree.arena[idworker].val = Component::Worker(WorkerType::GPUWorker, time + task.processing_time_gpu); task.processing_time_gpu},
                        };
                        //println!("{:#?} {:#?} {:#?} {:#?}", task.idtask, time, processing_time, idworker);
                        let tt = time + processing_time;
                        surface += processing_time;
                        if tt > makespan { makespan = tt;}
                        events.push(EventType::TryPull(idworker), - (time + processing_time));
                        match tree.arena[idworker].parent {
                            None => None,
                            Some(idparent) => events.push(EventType::TryPush(idparent), -time),
                        };
                    },
                }
            },
            Some((EventType::TryPush(idnode), date)) => 
            {
                time = -date;
                match tree.arena[idnode].val {
                    Component::Broker(SchedulingPolicy::eager) => 
                        match tree.arena[idnode].reservoir_content.pop() {
                            None => (),
                            Some(task) => 
                            {
                                let idcpu = tree.arena[idnode].children[0];
                                let idgpu = tree.arena[idnode].children[1];
                                let gcharge = tree.arena[idgpu].reservoir_content.len() as i32;
                                let ccharge = tree.arena[idcpu].reservoir_content.len() as i32;
                                match (gcharge  < tree.arena[idgpu].capacity, ccharge < tree.arena[idcpu].capacity) {
                                    (true, true) => 
                                        if task.processing_time_cpu < task.processing_time_gpu {
                                            tree.arena[idcpu].reservoir_content.push(task);
                                            events.push(EventType::TryPush(idnode), -time); events.push(EventType::TryPush(idcpu), -time);
                                        } else {
                                            tree.arena[idgpu].reservoir_content.push(task);
                                            events.push(EventType::TryPush(idnode), -time); events.push(EventType::TryPush(idgpu), -time);
                                        }
                                    ,
                                    (true, false) => { tree.arena[idgpu].reservoir_content.push(task); 
                                        tree.arena[idgpu].estimated_availability_time = std::cmp::max(tree.arena[idgpu].estimated_availability_time, time) + task.processing_time_gpu;
                                        events.push(EventType::TryPush(idnode), -time); events.push(EventType::TryPush(idgpu), -time); },
                                    (false, true) => { tree.arena[idcpu].reservoir_content.push(task); tree.arena[idcpu].estimated_availability_time += task.processing_time_cpu;
                                        tree.arena[idgpu].estimated_availability_time = std::cmp::max(tree.arena[idcpu].estimated_availability_time, time) + task.processing_time_cpu;
                                        events.push(EventType::TryPush(idnode), -time); events.push(EventType::TryPush(idcpu), -time);},
                                    (false, false) => { tree.arena[idnode].reservoir_content.push(task); 
                                        tree.arena[idnode].estimated_availability_time = std::cmp::max(tree.arena[idnode].estimated_availability_time, time) + task.processing_time_cpu;
                                        tree.arena[idnode].estimated_availability_time += task.processing_time_cpu; }
                                }
                            }
                        },
                    Component::Broker(SchedulingPolicy::ListScheduling) => 
                        match tree.arena[idnode].reservoir_content.pop() {
                            None => (),
                            Some(task) => 
                            {
                                let m = tree.arena[idnode].children.len();
                                let mut available_date_proc: Vec<(i32, usize)> = Vec::new();
                                let mut id:usize = 0usize;
                                for i in 0..m {
                                    let charge = tree.arena[tree.arena[idnode].children[i]].reservoir_content.len();
                                    if charge < tree.arena[tree.arena[idnode].children[i]].capacity as usize {
                                        let idchild = tree.arena[idnode].children[i];

                                        let processing_time = match  tree.arena[idchild].val {
                                            Component::Worker(WorkerType::CPUWorker, _) => task.processing_time_cpu,
                                            Component::Worker(WorkerType::GPUWorker, _) => task.processing_time_gpu,
                                            _ => (-1), //panic
                                        };

                                        if tree.arena[idchild].estimated_availability_time < time
                                        { tree.arena[idchild].estimated_availability_time = time; }
                                        available_date_proc.push((tree.arena[tree.arena[idnode].children[i]].estimated_availability_time + processing_time, id));
                                    }
                                    id = id + 1usize;
                                }
                                available_date_proc.sort();
                                if available_date_proc.len() > 0 {
                                    let idchild = tree.arena[idnode].children[available_date_proc[0].1];
                                    tree.arena[idchild].reservoir_content.push(task);
                                    tree.arena[idchild].estimated_availability_time = available_date_proc[0].0;
                                    events.push(EventType::TryPush(idnode), -time);
                                    events.push(EventType::TryPush(idchild), -time);
                                } else {
                                    tree.arena[idnode].reservoir_content.push(task);
                                }
                            }
                        }
                    ,
                    Component::Worker(_, availability_date) => 
                        if availability_date <= time {
                            events.push(EventType::TryPull(idnode), -time);
                        } else {
                            events.push(EventType::TryPull(idnode), -availability_date);
                        },
                }
            },
            Some((EventType::CanPush(idnode), date)) =>
            {
                time = -date;
                if tree.arena[idnode].reservoir_content.len() == 0 {
                    match tree.arena[idnode].parent {
                        None => None,
                        Some(idparent) => events.push(EventType::CanPush(idparent), -time)
                    };
                } else {
                    events.push(EventType::TryPush(idnode), -time);
                }
            }
        }
    }
    println!("{:#?} {:#?} {:#?} {:#?} {:#?} {:#?}", number_tasks, capacity, number_cpuworkers, number_gpuworkers, makespan, surface);
}
