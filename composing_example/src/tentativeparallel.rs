use priority_queue::PriorityQueue;

#[derive(PartialEq, Hash)]
enum WorkerType {
    CPUWorker,
    GPUWorker
}

#[derive(PartialEq, Hash)]
struct Task { wtype: WorkerType, processing_time_cpu : i32, number_cpus : i32, processing_time_gpu : i32, number_gpus : i32}

struct Reservoir { content: Vec<Task>, charge: i32}

struct Worker { availability_date : i32}

//Evenement

#[derive(PartialEq, Hash)]
enum EventType {
    NewTask(Task),
    EndOfWork(i32),
}

impl Eq for EventType {}

fn partitioner<'a>(entryReservoir: &'a mut Reservoir, cpuReservoir: &'a mut Reservoir, gpuReservoir: &'a mut Reservoir) -> Option<&'a mut Reservoir> {
    match entryReservoir.content.pop() {
        None => None,
        Some(task) => {
            let cpucharge = task.processing_time_cpu * task.number_cpus; 
            let gpucharge = task.processing_time_gpu * task.number_gpus;
            if cpucharge < gpucharge {
                cpuReservoir.content.push(task);
                Some(cpuReservoir)
            }
            else {
                gpuReservoir.content.push(task);
                Some(gpuReservoir)
            }
        },
    }
}

fn parallel_task_scheduling<'a>(time: i32, entryReservoir: &'a mut Reservoir, outReservoirs: Vec<&'a mut Reservoir>, wtype: WorkerType) -> Vec<i32> {
    let n = entryReservoir.content.len();
    let m = outReservoirs.len();
    //We keep in memory the next available date and the id of each processor
    //We want it sorted at every moment 
    let mut available_date_proc: Vec<(i32, usize)> = Vec::new();
    let mut id:usize = 0usize;
    for i in 0..m {
        available_date_proc.push((time + outReservoirs[i].charge, id));
        id = id + 1usize;
    }

    let mut tasks: Vec<i32> = Vec::new();
    let mut beginning_dates: Vec<i32> = Vec::new();
    let mut used_processors: Vec<Vec<i32>> = Vec::new();
    //We assign n times a task

    //We choose the task to assign
    //To do this, we take the minimum possible date 
    let mut dmin = i32::MAX; //initialized to infinity
    let mut task_to_assign = 0;
    for i in 0..n {
        let ncores:usize = entryReservoir.content[i].number_cpus as usize;
        //maximum of the first number of cores smallest possible date of processors
        let possible_beginning_date = available_date_proc[ncores].0;
        if possible_beginning_date < dmin {
            dmin = possible_beginning_date;
            task_to_assign = i;
        }
    }
    //We assign this task
    let mut processing_time = entryReservoir.content[task_to_assign].processing_time_cpu;
    let mut number_cores = entryReservoir.content[task_to_assign].number_cpus;
    if wtype == WorkerType::GPUWorker { 
        processing_time = entryReservoir.content[task_to_assign].processing_time_gpu;
        number_cores = entryReservoir.content[task_to_assign].number_cpus;
    }
    let new_date:i32 = processing_time + dmin;


    let mut used_proc_by: Vec<i32> = Vec::new();
    for i in 0..number_cores {
        used_proc_by.push(available_date_proc[i as usize].1 as i32);
    }
    used_proc_by
}


fn list_scheduler(entryReservoir: Reservoir, outReservoirs: Vec<Reservoir>) {

}

fn main() {
    let mut entryReservoir: Reservoir = Reservoir { content: Vec::new(), charge: 0 };
    let mut cpuReservoir: Reservoir = Reservoir { content: Vec::new(), charge: 0};
    let mut gpuReservoir: Reservoir = Reservoir { content: Vec::new(), charge: 0};

    let mut cpuworkerReservoirs: Vec<&mut Reservoir> = Vec::new();
    let mut cpuworkers: Vec<Worker> = Vec::new();
    let mut gpuworkerReservoirs: Vec<&mut Reservoir> = Vec::new();
    let mut gpuworkers: Vec<Worker> = Vec::new();

    //Simulation
    let mut events: PriorityQueue<EventType, i32> = PriorityQueue::new();
    let mut t:i32 = 0;
    //Simulateur evenements discrets
    while(! events.is_empty()) {
        match events.pop() {
            Some((EventType::NewTask(task), date)) => 
            (
                t = date,
                entryReservoir.content.push(task),
                match partitioner(&mut entryReservoir, &mut cpuReservoir, &mut gpuReservoir) {
                    None => (),
                    Some(reservoir) => 
                    for workerId in parallel_task_scheduling(t, reservoir, cpuworkerReservoirs, WorkerType::CPUWorker).iter_mut() {
                        
                    }
                }

            ),
            Some((EventType::EndOfWork(id), date)) => t = date,
            None => (),
        }
    }
}
