from tkinter import *
import random

#A besoin d'un argument lors de l'execution sur le premier id des unites de calcul
#Doit lire sur l'entree le nombre de taches, de cpus, gpus
#et chaque tache sous la forme idtache, date_debut, duree, idcoeur
#Affiche l'assignation

#Des couleurs au hasard
colors = ["#ba9aea", "#e1b101", "#be0e0a", "#b1aa41", "#e1b1e1", "#7e18ae", "#4be415", "#2020aa"]
colors += ["#ba1acc", "#ab1de0", "#427456", "#3aba2c", "#abcd20", "#423456", "#ba4acc", "#ebcde0"]


n = int(input())
ncpus = int(input())
ngpus = int(input())

#du tkinter
root = Tk()
size = 50
H = size * (ncpus + ngpus) + 25
T = 22
root.geometry(str(T*size)+"x"+str(H))
canvas = Canvas(root, width=2000, height=H)
canvas.pack()
#Creation des colonnes pour mieux compter les temps
canvas.create_rectangle(0 * size, 0, T * size, (ncpus + ngpus) * size + 20, fill="gray")
for t in range(T//2):
   canvas.create_rectangle(2*t*size, 0, (2*t+1) * size, H, fill="lightgray", width=0)
   
for i in range(n):
   #lecture de chaque tache et affichage
   idtask, time, duration, id = map(int, input().split())
   id -= int(sys.argv[1])
   height = size*id
   if id >= ncpus:
      height += 20
   color = colors[idtask]
   canvas.create_rectangle(time * size, height, (time + duration) * size, height + size,fill=color, width = 3)
   canvas.create_text((time + duration/2) * size, height + size/2, text = str(idtask))
root.mainloop()
